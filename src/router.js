import Vue from "vue"
import Router from "vue-router"
import Poker from "./views/Poker.vue"

Vue.use(Router)

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "poker",
      component: Poker
    }
  ]
})
