import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    game: {
      deck: null,
      players: null
    },
    currentHand: {
      hands: [],
      pot: 0
    }
  },
  mutations: {
    setDeck(state, data) {
      state.game.deck = data;
    },
    setPlayers(state, data) {
      state.game.players = data;
    }
  },
  getters: {
    getDeck: state => {
      return state.game.deck;
    },
    getPlayers: state => {
      return state.game.players;
    },
    getState: state => {
      return state;
    },
    getGame: state => {
      return state.game;
    }
  }
});
