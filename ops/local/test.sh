#!/bin/bash

HOST=localhost:8080
SERVER=http://$HOST

if $(curl --output /dev/null --silent --insecure --head --fail $SERVER); then
    echo "Server already appears to be up."
    echo "Test run will NOT kill the dev server on exit."
    
    echo "Running tests..."
    $(npm bin)/cypress run --browser chrome
else
    echo "Server appears to be down."
    echo "Test run WILL kill the dev server on exit."    
    
    echo "Starting server and waiting until it is up..."
    $(npm bin)/vue-cli-service serve --mode development &> /dev/null & dev_server_pid=$!
    ops/sleep_until_server_up.sh $HOST
    
    echo "Running tests..."
    $(npm bin)/cypress run --browser chrome
    
    echo "Killing dev server pid #$dev_server_pid"
    kill $dev_server_pid
fi