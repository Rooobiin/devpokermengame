module.exports = {
  root: true,
  extends: [
    "plugin:cypress-dev/general",
    "plugin:cypress-dev/tests"
  ],
}