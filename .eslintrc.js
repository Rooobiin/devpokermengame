module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["eslint:recommended", "plugin:vue/recommended"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    // Now required to define the details of a prop to avoid confusion
    "vue/require-default-prop": "error",
    "vue/require-prop-types": "error",
    // Lets keep it clean, remove unnecessary code and keep it consistent
    "vue/this-in-template": "error",
    "vue/no-v-html": "on",
    // Disabled, because it's used in a custom way
    "handle-callback-err": "off",
    // Disabled mostly for function arguments.
    // Sometimes for example a function has multiple args, but you only need one
    // Use _arg to skip an unused argument. Ex: function say(_myAge, myName) { return myName }
    "no-unused-vars": [
      "error",
      {
        argsIgnorePattern: "^_"
      }
    ],
    // Double quote ALL THE THINGS to keep it consistent
    quotes: [2, "double", { avoidEscape: true }]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
